
# OPTIONS=-O2
OPTIONS=-g3

ARCH=-m32

rm -f *.o dcscomp

gcc $ARCH -o cleanup.o -c cleanup.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o error.o -c error.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o lex.o -c lex.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o memory.o -c memory.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o opcode.o -c opcode.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o parse.o -c parse.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o pretty.o -c pretty.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o symbol.o -c symbol.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o tree.o -c tree.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o weed.o -c weed.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH -o standalone_main.o -c standalone_main.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc $ARCH $OPTIONS -o dcscomp cleanup.o error.o lex.o memory.o opcode.o parse.o \
pretty.o symbol.o tree.o weed.o standalone_main.o -lc
echo "Success"
