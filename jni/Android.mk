LOCAL_PATH:= $(call my-dir)

# Reverb library
include $(CLEAR_VARS)

# LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES:= \
../cleanup.c \
../error.c \
../lex.c \
../main.c \
../memory.c \
../opcode.c \
../parse.c \
../pretty.c \
../symbol.c \
../tree.c \
../weed.c

LOCAL_MODULE:= libdcscomp

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/

LOCAL_CFLAGS += -Wall -Wextra -Wcast-align -Wno-switch -Wno-unused-parameter -Wno-unused-function -Wno-pointer-sign

LOCAL_LDLIBS := -ldl

include $(BUILD_SHARED_LIBRARY)

