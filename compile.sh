
OPTIONS=-O2
# OPTIONS=-g3

gcc -o cleanup.o -c cleanup.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o error.o -c error.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o lex.o -c lex.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o memory.o -c memory.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o opcode.o -c opcode.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o parse.o -c parse.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o pretty.o -c pretty.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o symbol.o -c symbol.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o tree.o -c tree.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o weed.o -c weed.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -o main.o -c main.c $OPTIONS -fPIC -Wall -Wextra || exit;
gcc -shared -Wl,-soname,libdcscomp.so $OPTIONS -o libdcscomp.so cleanup.o error.o lex.o memory.o opcode.o parse.o \
pretty.o symbol.o tree.o weed.o main.o -lc
echo "Success"

