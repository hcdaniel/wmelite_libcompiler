#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "dcscript.h"
#include "error.h"
#include "memory.h"
#include "pretty.h"
#include "tree.h"
#include "weed.h"
#include "symbol.h"
#include "opcode.h"
#include "cleanup.h"


CALLBACKS callbacks;
void* ExtData;

/* Prototypes for bison */
typedef struct yy_buffer_state *YY_BUFFER_STATE;
void yyparse();
YY_BUFFER_STATE yy_scan_bytes (const char *bytes, int len);
void yy_delete_buffer (YY_BUFFER_STATE b);


int yywrap(){
	return 1;
} 


TExternalSymbol* ExternalVars=NULL;
TExternalSymbol* ExternalFuncs=NULL;


/* The declaration of the extern variable from lang.y */
struct SCRIPTCOLLECTION *thescriptcollection;


//////////////////////////////////////////////////////////////////////////
BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved){
	switch (ul_reason_for_call){
		case DLL_PROCESS_ATTACH:
			callbacks.Dll_AddError = NULL;
			callbacks.Dll_CloseFile = NULL;
			callbacks.Dll_LoadFile = NULL;
			callbacks.Dll_ParseElement = NULL;
			break;

		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

extern int errors;
extern int buffer_size;
extern DWORD BufferSize;
extern int include_stack_ptr;
extern int lineno;
extern int commentDepth;

//////////////////////////////////////////////////////////////////////////
void FinalCleanup(){

	TExternalSymbol* sym;

	while(ExternalVars!=NULL){
		sym = ExternalVars;
		ExternalVars = (TExternalSymbol*)ExternalVars->next;
		free(sym->name);
		free(sym);
	}
	while(ExternalFuncs!=NULL){
		sym = ExternalFuncs;
		ExternalFuncs = (TExternalSymbol*)ExternalFuncs->next;
		free(sym->name);
		free(sym);
	}
	
	cleanupSCRIPTCOLLECTION(thescriptcollection);
	thescriptcollection = NULL;

	ExternalVars = NULL;
	ExternalFuncs = NULL;

	errors=0;
	buffer_size=0;
	BufferSize=0;
	include_stack_ptr=0;
	lineno=0;
	commentDepth=0;
}


//////////////////////////////////////////////////////////////////////////
// EXPORTED FUNCTIONS
//////////////////////////////////////////////////////////////////////////
__declspec(dllexport) BYTE* CompileBuffer(BYTE* Buffer, char* Source, DWORD BufferSize, DWORD* CompiledSize){
	
	YY_BUFFER_STATE state;
	BYTE* ret = NULL;

	if(Buffer==NULL){
		if(CompiledSize)*CompiledSize = 0;
		return NULL;
	}

	state = yy_scan_bytes(Buffer, BufferSize);
	lineno=1;
	yyparse();
	yy_delete_buffer(state);
	if(errorCheck()) goto finish_error;

	printf("1..");

	weedSCRIPTCOLLECTION(thescriptcollection);
	if(errorCheck()) goto finish_error;
	
	printf("2..");

	symSCRIPTCOLLECTION(thescriptcollection);
	if(errorCheck()) goto finish_error;
	
	printf("3..");

	ret = opcodeSCRIPTCOLLECTION(thescriptcollection, Source, CompiledSize);
	if(errorCheck()) goto finish_error;

	printf("done\n");

	prettySCRIPTCOLLECTION(thescriptcollection);

	FinalCleanup();
	return ret;


finish_error:
	if(ret!=NULL) free(ret);
	*CompiledSize = 0;
	FinalCleanup();
	printf("Compilation error!\n");
	return NULL;
}


//////////////////////////////////////////////////////////////////////////
__declspec(dllexport) BYTE* CompileFile(char* Filename, DWORD* CompiledSize){
	BYTE* Buffer=NULL;
	DWORD BufferSize=0;
	BYTE* ret;

	if(callbacks.Dll_LoadFile){
		Buffer = callbacks.Dll_LoadFile(ExtData, Filename, &BufferSize);
	}

	if(Buffer==NULL){
		if(callbacks.Dll_AddError) callbacks.Dll_AddError(ExtData, 0, "Error opening script.");
		if(CompiledSize) *CompiledSize = 0;
		return NULL;
	}
	else{
		if(IsUTF8(Buffer, BufferSize)) ret = CompileBuffer(Buffer+3, Filename, BufferSize-3, CompiledSize);
		else ret = CompileBuffer(Buffer, Filename, BufferSize, CompiledSize);

		if(callbacks.Dll_CloseFile) callbacks.Dll_CloseFile(ExtData, Buffer);
		return ret;
	}
}


//////////////////////////////////////////////////////////////////////////
__declspec(dllexport) void ReleaseBuffer(unsigned char* Buffer){

	free(Buffer);
}


//////////////////////////////////////////////////////////////////////////
__declspec(dllexport) void SetCallbacks(CALLBACKS* in_callbacks, void* in_data){
	memcpy(&callbacks, in_callbacks, sizeof(CALLBACKS));
	ExtData = in_data;
}


//////////////////////////////////////////////////////////////////////////
__declspec(dllexport) int DefineFunction(char* Name){
	TExternalSymbol* sym;
	sym = NEW(TExternalSymbol);
	sym->kind = functionSym;
	sym->name = malloc(strlen(Name)+1);
	strcpy(sym->name, Name);
	
	sym->next = (struct TExternalSymbol*)ExternalFuncs;
	ExternalFuncs = sym;

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////
__declspec(dllexport) int DefineVariable(char* Name){

	TExternalSymbol* sym;
	sym = NEW(TExternalSymbol);
	sym->kind = declSym;
	sym->name = malloc(strlen(Name)+1);
	strcpy(sym->name, Name);
	
	sym->next = (struct TExternalSymbol*)ExternalVars;
	ExternalVars = sym;

	return TRUE;	
}

static void unify_separators(char *buf)
{
	int len = strlen(buf);

	for (int i = 0; i < len; i++)
	{
		if (buf[i] == '\\')
		{
			buf[i] = '/';
		}
	}
}


//////////////////////////////////////////////////////////////////////////
int IsUTF8(BYTE* Buffer, int BufferSize)
{
	if(BufferSize>3 && Buffer[0]==0xEF && Buffer[1]==0xBB && Buffer[2]==0xBF) return 1;
	else return 0;
}

BYTE* my_DLL_LOAD_FILE(void* Data, char* Filename, DWORD* Size)
{
	unify_separators(Filename);

	FILE *f = fopen(Filename, "rb");

	if (f == NULL)
	{
		printf("Could not open file %s!\n", Filename);
	}

	size_t len;
	BYTE *buffer = malloc(100000);

	len = fread(buffer, 1, 100000, f);

	printf("File %s len=%d.\n", Filename, len);

	fclose(f);

	*Size = len;

	return buffer;
}

void my_DLL_CLOSE_FILE(void* Data, BYTE* Buffer)
{
	free(Buffer);
}

void my_DLL_ADD_ERROR(void* Data, int Line, char* Text)
{
	printf("Error in line %d: %s.\n", Line, Text);
}

void my_DLL_PARSE_ELEMENT(void* Data, int Line, int Type, void* ElementData)
{
	printf("No idea what to do here! Line=%d Type=%d\n", Line, Type);
}

int main(int argc, char **argv)
{
	BYTE* ret;
	DWORD CompiledSize;

	char inFileName[10000];
	char outFileName[10000];

	callbacks.Dll_AddError = my_DLL_ADD_ERROR;
	callbacks.Dll_CloseFile = my_DLL_CLOSE_FILE;
	callbacks.Dll_LoadFile = my_DLL_LOAD_FILE;

	callbacks.Dll_ParseElement = my_DLL_PARSE_ELEMENT;

	DefineFunction("LOG");
	DefineFunction("String");
	DefineFunction("MemBuffer");
	DefineFunction("File");
	DefineFunction("Date");
	DefineFunction("Array");
	DefineFunction("TcpClient");
	DefineFunction("Object");

	DefineFunction("Sleep");
	DefineFunction("WaitFor");
	DefineFunction("Random");
	DefineFunction("SetScriptTimeSlice");
	DefineFunction("MakeRGBA");
	DefineFunction("MakeRGB");
	DefineFunction("MakeHSL");
	DefineFunction("RGB");
	DefineFunction("GetRValue");
	DefineFunction("GetGValue");
	DefineFunction("GetBValue");
	DefineFunction("GetAValue");
	DefineFunction("GetHValue");
	DefineFunction("GetSValue");
	DefineFunction("GetLValue");
	DefineFunction("Debug");

	DefineFunction("ToString");
	DefineFunction("ToInt");
	DefineFunction("ToBool");
	DefineFunction("ToFloat");

	DefineVariable("Game");
	DefineVariable("Math");
	DefineVariable("Directory");
	DefineVariable("self");
	DefineVariable("this");

	strcpy(inFileName, argv[1]);
	unify_separators(inFileName);

	printf("Compiling file %s.\n", inFileName);

	ret = CompileFile(inFileName, &CompiledSize);

	printf("Compiled script size=%d.\n", CompiledSize);

	strcpy(outFileName, inFileName);
	strcat(outFileName, ".bin");

	FILE *f = fopen(outFileName, "wb");
	fwrite(ret, 1, CompiledSize, f);
	fclose(f);

	return 0;
}
